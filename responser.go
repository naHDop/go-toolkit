package go_toolkit

import (
	"net/http"
	"time"
)

type GenericError struct {
	Message string `json:"message"`
}

func newGenericError(err error) *GenericError {
	if err == nil {
		return nil
	}
	return &GenericError{
		Message: err.Error(),
	}
}

type ResponseCode string

type responser struct {
}

type Responser interface {
	NewResponse(data any, err error, code ResponseCode) (response Response)
}

type Response struct {
	Status    int           `json:"status"`
	Message   string        `json:"message"`
	Timestamp time.Time     `json:"timestamp"`
	Data      any           `json:"data"`
	Error     *GenericError `json:"error"`
}

func NewResponser() Responser {
	return &responser{}
}

const (
	OK           ResponseCode = "100001"
	BAD_REQUEST  ResponseCode = "100002"
	FAIL         ResponseCode = "100003"
	UNAUTHORIZED ResponseCode = "100004"
	NOT_FOUND    ResponseCode = "100005"
)

var codeToMessageMap = map[ResponseCode]string{
	OK:           "OK",
	BAD_REQUEST:  "Bad request",
	FAIL:         "Failed",
	UNAUTHORIZED: "Unauthorized",
	NOT_FOUND:    "Resource not found",
}

var codeToStatusMap = map[ResponseCode]int{
	OK:           http.StatusOK,
	BAD_REQUEST:  http.StatusBadRequest,
	FAIL:         http.StatusInternalServerError,
	UNAUTHORIZED: http.StatusUnauthorized,
	NOT_FOUND:    http.StatusNotFound,
}

func (r *responser) NewResponse(data any, err error, code ResponseCode) (response Response) {
	response.Status = codeToStatusMap[code]
	response.Data = data
	response.Timestamp = time.Now()
	response.Message = codeToMessageMap[code]
	response.Error = newGenericError(err)
	return
}
