module gitlab.com/naHDop/go-toolkit

go 1.21.1

require (
	github.com/aead/chacha20poly1305 v0.0.0-20201124145622-1a5aba2a8b29
	github.com/golang-jwt/jwt/v5 v5.2.0
	github.com/google/uuid v1.6.0
	github.com/o1egl/paseto v1.0.0
	go.uber.org/zap v1.26.0
	golang.org/x/crypto v0.18.0
)

require (
	github.com/aead/chacha20 v0.0.0-20180709150244-8b13a72661da // indirect
	github.com/aead/poly1305 v0.0.0-20180717145839-3fee0db0b635 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
)
