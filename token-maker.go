package go_toolkit

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
)

var (
	ErrExpiredToken = errors.New("token has expired")
	ErrInvalidToken = errors.New("invalid token")
)

type Payload struct {
	jwt.RegisteredClaims
	ID     uuid.UUID `json:"id"`
	UserId uuid.UUID `json:"userId"`
}

func CreatePayload(userPayload Payload, duration time.Duration) (*Payload, error) {
	tokenId, err := uuid.NewUUID()
	if err != nil {
		return nil, err
	}

	payload := &Payload{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(duration)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			NotBefore: jwt.NewNumericDate(time.Now()),
		},
		ID:     tokenId,
		UserId: userPayload.UserId,
	}

	return payload, nil
}
func (p *Payload) Valid() error {
	if time.Now().After(p.RegisteredClaims.ExpiresAt.Time) {
		return ErrExpiredToken
	}

	return nil
}

type TokenMaker interface {
	CreateToken(userPayload Payload, duration time.Duration) (string, error)
	VerifyToken(token string) (*Payload, error)
}
