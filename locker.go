package go_toolkit

import "sync"

type Locker struct {
	mu    sync.Mutex
	locks map[string]*sync.Mutex
}

func NewLocker() *Locker {
	return &Locker{
		locks: make(map[string]*sync.Mutex),
	}
}

func (l *Locker) Lock(id string) {
	l.mu.Lock()
	if _, ok := l.locks[id]; !ok {
		l.locks[id] = &sync.Mutex{}
	}
	l.mu.Unlock()

	l.locks[id].Lock()
}

func (l *Locker) Unlock(id string) {
	l.mu.Lock()
	defer l.mu.Unlock()

	if lock, ok := l.locks[id]; ok {
		lock.Unlock()
		delete(l.locks, id)
	}
}

func (l *Locker) IsLocked(id string) bool {
	l.mu.Lock()
	defer l.mu.Unlock()

	_, exists := l.locks[id]
	return exists
}
