package go_toolkit

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type JWTToken struct {
	secret string
}

const minSecretSize = 32

func NewJWTMaker(secret string) (TokenMaker, error) {
	if len(secret) < minSecretSize {
		return nil, errors.New("to short secret size")
	}

	return &JWTToken{secret}, nil
}

func (tm *JWTToken) CreateToken(userPayload Payload, duration time.Duration) (string, error) {
	payload, err := CreatePayload(userPayload, duration)
	if err != nil {
		return "", err
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)
	return token.SignedString([]byte(tm.secret))
}

func (tm *JWTToken) VerifyToken(token string) (*Payload, error) {
	keyFunc := func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, ErrInvalidToken
		}

		return []byte(tm.secret), nil
	}

	jwtToken, err := jwt.ParseWithClaims(token, &Payload{}, keyFunc)
	if err != nil {
		return nil, err
	}

	if payload, ok := jwtToken.Claims.(*Payload); ok && jwtToken.Valid {
		return payload, nil
	} else {
		return nil, ErrInvalidToken
	}
}
